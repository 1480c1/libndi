project('libndi', 'c',
  version: '0.0.1',
  default_options: ['c_std=c99',
                      'warning_level=3',
                      'buildtype=release',
                      'b_ndebug=if-release'],
  meson_version: '>= 0.47.0')

cc = meson.get_compiler('c')

# API/ABI version (soversion) for the library
ndi_soname_version       = '0.0.1'

add_project_arguments('-D_POSIX_C_SOURCE=200809L', language : 'c')

#
# The NDI Library
#

libavutil_dep = dependency('libavutil', required: true)


poll_src = '''
#include <stddef.h>
#ifdef _WIN32
#include <winsock2.h>
#include <windows.h>
# if _WIN32_WINNT < 0x600
#  error Needs vista+
# endif
# if defined(_MSC_VER)
#   error
# endif
#else
#include <poll.h>
#endif
int main() {
  poll(NULL, 0, 0);
}
'''

if cc.links(poll_src)
    add_project_arguments('-DHAVE_POLL=1', language: 'c')
endif

# mDNS discovery specifics
ndi_discovery_sources = []
ndi_discovery_deps = []

microdns_dep = dependency('microdns', required: get_option('microdns'))
if microdns_dep.found()
  threads_dep = dependency('threads', required: false)
  ndi_discovery_sources += files('libndi_discovery_microdns.c')
  ndi_discovery_deps += [threads_dep, microdns_dep]
endif

libndi = library('ndi',
    'libndi.c',
    'libndi.h',
    ndi_discovery_sources,
    version: ndi_soname_version,
    install: true,
    dependencies: [libavutil_dep, ndi_discovery_deps])

install_headers('libndi.h')

#
# Example Executable
#

libavcodec_dep = dependency('libavcodec', required: true)

executable('ndi',
           'ndi.c',
           install: true,
           link_with: [libndi],
           dependencies: [libavcodec_dep, libavutil_dep])
